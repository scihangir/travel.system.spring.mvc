package com.cihangir.controller;

import com.cihangir.model.Admin;
import com.cihangir.model.Travel;
import com.cihangir.model.User;
import com.cihangir.service.impl.AuthenticationService;
import com.cihangir.service.impl.AdminService;
import com.cihangir.service.impl.TravelService;
import com.cihangir.service.impl.UserService;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private AdminService adminService;

    @Autowired
    private UserService userService;

    @Autowired
    private TravelService travelService;





    @RequestMapping("/usermanagement")
    public String getUserList(Model model) {

        // get users from the service
        List<User> userList=userService.getAllUser();

        //add the user which authenticatedAdmin to the model
        model.addAttribute("authenticatedAdmin",authenticationService.getAuthenticatedAdmin().getUsername() );

        // add the users to the model
        model.addAttribute("users", userList);

        return "Admin/user-list";
    }

    @RequestMapping(value = "/showFormForAdd", method = RequestMethod.GET)
    public String showFormForAddUser(Model theModel) {

        // create model attribute to bind form data
        User theUser=new User();

        //add the user which authenticatedAdmin to the model
        theModel.addAttribute("authenticatedAdmin",authenticationService.getAuthenticatedAdmin().getUsername() );

        theModel.addAttribute("newUser", theUser);

        return "Admin/user-define";
    }

    @RequestMapping(value = "/showFormForEdit", method = RequestMethod.GET)
    public String showFormForEditUser(@RequestParam("userId") Long theId, Model theModel) {

        // get the user from our service
        User theUser=userService.findUserById(theId);

        //add the user which authenticatedAdmin to the model
        theModel.addAttribute("authenticatedAdmin",authenticationService.getAuthenticatedAdmin().getUsername() );

        //add the user to the model
        theModel.addAttribute("newUser", theUser);


        return "Admin/user-define";
    }

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public String saveUser(@ModelAttribute("newUser") User theUser) {

        userService.saveUser(theUser);

        return "redirect:/admin/usermanagement";
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    public String deleteUser(@RequestParam("userId") Long theId) {

        //o kullaniciya ait seyahatler siliniyor...
        travelService.deleteTravelByUserId(theId);

        //delete the user
        userService.deleteUser(theId);


        return "redirect:/admin/usermanagement";
    }

    @RequestMapping(value = "/showFormForEditAdmin", method = RequestMethod.GET)
    public String showFormForEditAdmin(Model theModel) {

        //add the user which authenticatedAdmin to the model
        theModel.addAttribute("authenticatedAdmin",authenticationService.getAuthenticatedAdmin().getUsername() );

        // get the user from our service
        Admin theAdmin=adminService.loadAdmin(authenticationService.getAuthenticatedAdmin().getUsername());

        //add the user to the model
        theModel.addAttribute("editAdmin", theAdmin);


        return "Admin/admin-profile";
    }

    @RequestMapping(value = "/updateAdmin", method = RequestMethod.POST)
    public String updateAdmin(@ModelAttribute("editAdmin") Admin theAdmin) {

        adminService.updateAdmin(theAdmin);

        return "redirect:/admin/usermanagement";
    }

    @RequestMapping("/travelList")
    public String getTravelList(Model model) {

        // get travels from the service
        List<Travel> travelList=travelService.findAllTravels();

        //add the user which authenticatedAdmin to the model
        model.addAttribute("authenticatedAdmin",authenticationService.getAuthenticatedAdmin().getUsername() );

        // add the users to the model
        model.addAttribute("travels", travelList);

        return "Admin/travel-list";
    }

    @RequestMapping(value = "/search")
    public String searchTravel(@RequestParam("sDate") @DateTimeFormat(pattern="dd.MM.yyyy") Date d1, @RequestParam("eDate") @DateTimeFormat(pattern="dd.MM.yyyy") Date  d2, Model theModel) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        List<Travel> travelList = travelService.getTravelsBetweenDates(d1,d2);

        //add the user which authenticatedAdmin to the model
        theModel.addAttribute("authenticatedAdmin",authenticationService.getAuthenticatedAdmin().getUsername() );

        theModel.addAttribute("bTime",sdf.format(d1));
        theModel.addAttribute("fTime",sdf.format(d2));

        // add the users to the model
        theModel.addAttribute("travels", travelList);

        return "Admin/travel-list";
    }

    @RequestMapping(value = "/fillSelectBox",method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<User> fillSelectBox() {
        return userService.getAllUser();
    }

    @RequestMapping(value = "/selectUser", method = RequestMethod.GET  )
    public String getTravelByUserId(@RequestParam("userId") Long theId, Model theModel) {


        System.out.println("AdminController.getTravelByUserId");
        // get travels from the service
        List<Travel> travelList=travelService.getTravelByUserId(theId);

        //add the user which authenticatedAdmin to the model
        theModel.addAttribute("authenticatedAdmin",authenticationService.getAuthenticatedAdmin().getUsername() );

        // add the users to the model
        theModel.addAttribute("travels", travelList);


        return "Admin/travel-list";
    }











}
