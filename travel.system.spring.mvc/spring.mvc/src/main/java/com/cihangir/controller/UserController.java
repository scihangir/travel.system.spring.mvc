package com.cihangir.controller;

import com.cihangir.model.Travel;
import com.cihangir.model.User;
import com.cihangir.service.impl.AuthenticationService;
import com.cihangir.service.impl.TravelService;
import com.cihangir.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Table;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private TravelService travelService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationService authenticationService;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder ) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        webDataBinder.registerCustomEditor(Date.class,"startDate",new CustomDateEditor(sdf, false));
        webDataBinder.registerCustomEditor(Date.class,"endDate",new CustomDateEditor(sdf, false));

    }

    @RequestMapping(value = "/showFormForAdd", method = RequestMethod.GET)
    public String showFormForAddTravel(Model theModel) {

        // create model attribute to bind form data
        Travel theTravel=new Travel();

        //add the user which authenticatedAdmin to the model
        theModel.addAttribute("authenticatedUser",authenticationService.getAuthenticatedUser().getUsername() );

        theModel.addAttribute("newTravel", theTravel);


        return "User/travel-define";
    }

    @RequestMapping(value = "/showFormForEdit", method = RequestMethod.GET)
    public String showFormForEditTravel(@RequestParam("travelId") Long theId, Model theModel) {



        // create model attribute to bind form data
        Travel theTravel=travelService.findTravelById(theId);

        //add the user which authenticatedAdmin to the model
        theModel.addAttribute("authenticatedUser",authenticationService.getAuthenticatedUser().getUsername() );

        //add theTravel to the model
        theModel.addAttribute("newTravel", theTravel);

        return "User/travel-define";

    }

    @RequestMapping(value = "/deleteTravel", method = RequestMethod.GET)
    public String deleteTravel(@RequestParam("travelId") Long theId) {

        //delete the user
        travelService.deleteTravel(theId);

        return "redirect:/user/travelList";
    }

    @RequestMapping(value = "/saveTravel", method = RequestMethod.POST)
    public String saveTravel(@ModelAttribute("newTravel") Travel theTravel) {

        User theUser=userService.findUserByName(authenticationService.getAuthenticatedUser().getUsername());

        theTravel.setUser(theUser);
        travelService.saveTravel(theTravel);

        return "redirect:/user/travelList";
    }

    @RequestMapping("/travelList")
    public String getTravelList(Model model) {

        User theUser=authenticationService.getAuthenticatedUserInstance();
        // get travels from the service
        List<Travel> travelList=travelService.getTravelByUserId( theUser.getId());

        //add the user which authenticatedAdmin to the model
        model.addAttribute("authenticatedUser",authenticationService.getAuthenticatedUser().getUsername() );

        // add the users to the model
        model.addAttribute("travels", travelList);

        return "User/travel-list";
    }

    @RequestMapping(value = "/search")
    public String searchTravel(@RequestParam("sDate") @DateTimeFormat(pattern="dd.MM.yyyy") Date  d1, @RequestParam("eDate") @DateTimeFormat(pattern="dd.MM.yyyy") Date  d2, Model theModel) throws ParseException {

        System.out.println("Bu:"+d1);
        System.out.println("ŞU:"+d2);


        User theUser=authenticationService.getAuthenticatedUserInstance();
        System.out.println("UserController.searchTravel"+theUser.getUsername()+theUser.getId()+theUser.getPassword());

        //List<Travel> travelList = travelService.getTravelsBetweenDate(d1, d2);

        List<Travel> travelList = travelService.getTravelsBetweenDateByUserId(d1, d2, theUser.getId());


        // add the users to the model
        theModel.addAttribute("travels", travelList);

        return "User/travel-list";
    }

}
