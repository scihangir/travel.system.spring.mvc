package com.cihangir.dao;

import com.cihangir.model.Admin;
import com.cihangir.model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class AdminDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Admin loadAdmin(String username) {
        Criteria criteria=sessionFactory.getCurrentSession().createCriteria(Admin.class);
        criteria.add(Restrictions.eq("username", username));

        return (Admin) criteria.uniqueResult();
    }

    public void editAdmin(Admin theAdmin) {

        // get current hibernate session
        Session currentSession=sessionFactory.getCurrentSession();

        currentSession.update(theAdmin);;

    }
}
