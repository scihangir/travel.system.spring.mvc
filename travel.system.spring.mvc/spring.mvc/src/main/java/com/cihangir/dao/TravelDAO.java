package com.cihangir.dao;

import com.cihangir.model.Travel;
import com.cihangir.model.User;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public class TravelDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public List<Travel> findTravelByUserId(Long theId) {

        // get current hibernate session
        Session currentSession=sessionFactory.getCurrentSession();

//        Criteria criteria = currentSession.createCriteria(Travel.class)
//        .add(Restrictions.eq("user.id", theId));
//
//        return criteria.list();

        // create a query  .
        Query query = currentSession.createQuery("from Travel  where user_id = :id ");
        query.setParameter("id", theId);

        List<Travel> list = query.list();

        // return the result
        return list;
    }

    public void saveTravel(Travel theTravel) {
        // get current hibernate session
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(theTravel);
    }

    public void deleteTravel(Long theId) {

        // get current hibernate session
        Session currentSession=sessionFactory.getCurrentSession();

        //delete object with primary key
        Query theQuery=currentSession.createQuery("delete from Travel where id=:travelId");
        theQuery.setParameter("travelId",theId);

        theQuery.executeUpdate();

    }

    public Travel getTravelById(Long theId) {
        // get the current hibernate session
        Session currentSession = sessionFactory.getCurrentSession();

        Criteria criteria=currentSession.createCriteria(Travel.class);
        criteria.add(Restrictions.eq("id", theId));

        // return the result
        return (Travel) criteria.uniqueResult();
    }

    public List<Travel> findTravelsBetweenDatesWithUserId(Date d1, Date d2, Long id) {
        // get current hibernate session
        Session currentSession=sessionFactory.getCurrentSession();

        Criteria criteria = currentSession.createCriteria(Travel.class)
                .add(Restrictions.eq("user.id",id))
                .add(Restrictions.or(
                        Restrictions.isNull("startDate"),
                        Restrictions.ge("startDate", d1)))
                .add(Restrictions.or(
                        Restrictions.isNull("endDate"),
                        Restrictions.lt("endDate", d2)));

        List<Travel> travels=criteria.list();

        return travels;
    }

    public List<Travel> getAllTravels() {
        // get current hibernate session
        Session currentSession=sessionFactory.getCurrentSession();

        Criteria criteria=currentSession.createCriteria(Travel.class);
        List<Travel> travelList=criteria.list();

        return travelList;
    }

    public List<Travel> findTravelBetweenDate(Date d1, Date d2) {
        // get current hibernate session
        Session currentSession=sessionFactory.getCurrentSession();

        Criteria criteria = currentSession.createCriteria(Travel.class)
                .add(Restrictions.or(
                        Restrictions.isNull("startDate"),
                        Restrictions.ge("startDate", d1)))
                .add(Restrictions.or(
                        Restrictions.isNull("endDate"),
                        Restrictions.lt("endDate", d2)));

        List<Travel> travels=criteria.list();

        return travels;
    }

    public void deleteTravelWithUserId(Long theId) {
        // get current hibernate session
        Session currentSession=sessionFactory.getCurrentSession();

        //delete object with primary key
        Query theQuery=currentSession.createQuery("delete from Travel where user_id=:userId");
        theQuery.setParameter("userId",theId);

        theQuery.executeUpdate();
    }
}
