package com.cihangir.dao;

import com.cihangir.model.User;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;


@Repository
public class UserDAO {

    @Autowired
    private SessionFactory sessionFactory;


    public User findUserByName(String username) {

        // get current hibernate session
        Session currentSession=sessionFactory.getCurrentSession();

        Criteria criteria=currentSession.createCriteria(User.class);
        criteria.add(Restrictions.eq("username", username));

        // return the result
        return (User) criteria.uniqueResult();
    }

    public List<User> findAllUser() {

        // get current hibernate session
        Session currentSession=sessionFactory.getCurrentSession();


        Criteria criteria=currentSession.createCriteria(User.class);
        List<User> users=criteria.list();

        // return the results
        return users;
    }

    public User findUserById(Long theId) {

        // get the current hibernate session
        Session currentSession = sessionFactory.getCurrentSession();


        Criteria criteria=currentSession.createCriteria(User.class);
        criteria.add(Restrictions.eq("id", theId));

        // return the result
        return (User) criteria.uniqueResult();
    }

    public void deleteUser(Long theId) {

        // get current hibernate session
        Session currentSession=sessionFactory.getCurrentSession();

        //delete object with primary key
        Query theQuery=currentSession.createQuery("delete from User where id=:userId");
        theQuery.setParameter("userId",theId);

        theQuery.executeUpdate();

    }

    public void insertUser(User theUser) {
        // get current hibernate session
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(theUser);
    }
}
