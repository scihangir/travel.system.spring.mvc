package com.cihangir.security;

import com.cihangir.dao.AdminDAO;
import com.cihangir.dao.UserDAO;
import com.cihangir.model.Admin;
import com.cihangir.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



//The UserDetailsService interface is used to retrieve user-related data.


@Service
public class AdminDetailsService implements UserDetailsService {

    @Autowired
    private AdminDAO adminDAO;


    //loadUserByUsername() which finds a user entity based on the username and can be overridden
    //to customize the process of finding the user.
    //It is used by the DaoAuthenticationProvider to load details about the user during authentication.
    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //Kullanici adina bagli olarak veritabanindan kullanici bilgileri aliniyor...
        Admin admin=adminDAO.loadAdmin(username);


        if (admin == null) {
            throw new UsernameNotFoundException(username);
        }

        List<GrantedAuthority> authorities = buildUserAuthority("ROLE_ADMIN");
        return buildUserForAuthentication(admin, authorities);
    }

    // Converts com.cihangir.model.User user to
    // org.springframework.security.core.userdetails.User
    private org.springframework.security.core.userdetails.User buildUserForAuthentication(Admin admin, List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(admin.getUsername(), admin.getPassword(),true, true,true,true,authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(String role) {
        List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>();
        Result.add(new SimpleGrantedAuthority(role));

        return Result;
    }
}
