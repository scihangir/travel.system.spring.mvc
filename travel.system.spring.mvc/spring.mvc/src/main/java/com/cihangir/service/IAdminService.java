package com.cihangir.service;

import com.cihangir.model.Admin;
import com.cihangir.model.User;


public interface IAdminService {
    public Admin loadAdmin(String username);

    void updateAdmin(Admin theAdmin);
}
