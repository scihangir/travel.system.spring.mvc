package com.cihangir.service;

import com.cihangir.model.Admin;
import com.cihangir.model.User;


public interface IAuthenticationService {
    public Admin getAuthenticatedAdmin();
    public User getAuthenticatedUserInstance();


}
