package com.cihangir.service;

import com.cihangir.model.Travel;

import java.util.Date;
import java.util.List;


public interface ITravelService {

    List<Travel> getTravelByUserId(Long theId);

    void saveTravel(Travel theTravel);

    Travel findTravelById(Long theId);

    void deleteTravel(Long theId);

    List<Travel> getTravelsBetweenDateByUserId(Date d1, Date d2, Long id);

    List<Travel> findAllTravels();

    List<Travel> getTravelsBetweenDates(Date d1, Date d2);

    void deleteTravelByUserId(Long theId);
}
