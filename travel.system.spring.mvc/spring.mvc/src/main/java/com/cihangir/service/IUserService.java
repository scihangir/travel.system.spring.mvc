package com.cihangir.service;

import com.cihangir.model.Travel;
import com.cihangir.model.User;

import java.util.Date;
import java.util.List;


public interface IUserService {
    public User findUserByName(String username);
    public List<User> getAllUser();
    public User findUserById(Long id);
    public void saveUser(User user);
    public void deleteUser(Long theId);


}
