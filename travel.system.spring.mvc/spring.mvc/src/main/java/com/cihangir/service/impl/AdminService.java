package com.cihangir.service.impl;

import com.cihangir.dao.AdminDAO;
import com.cihangir.model.Admin;
import com.cihangir.model.User;
import com.cihangir.service.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;


@Service
@Transactional
public class AdminService implements IAdminService {

    @Autowired
    private AdminDAO adminDAO;

    public Admin loadAdmin(String username) {
        return adminDAO.loadAdmin(username);
    }

    @Override
    public void updateAdmin(Admin theAdmin) {
        adminDAO.editAdmin(theAdmin);
    }


}
