package com.cihangir.service.impl;

import com.cihangir.dao.AdminDAO;
import com.cihangir.dao.UserDAO;
import com.cihangir.model.Admin;
import com.cihangir.service.IAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class AuthenticationService implements IAuthenticationService {

    @Autowired
    private AdminDAO adminDAO;

    @Autowired
    private UserDAO userDAO;

    @Transactional
    public Admin getAuthenticatedAdmin() {
        String adminName = getAuthenticatedUser().getUsername();
        return adminDAO.loadAdmin(adminName);
    }

    @Transactional
    public com.cihangir.model.User getAuthenticatedUserInstance() {
        return userDAO.findUserByName(getAuthenticatedUser().getUsername());
    }


    @Transactional
    public org.springframework.security.core.userdetails.User getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
    }

}
