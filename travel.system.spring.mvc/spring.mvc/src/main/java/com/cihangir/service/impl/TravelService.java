package com.cihangir.service.impl;

import com.cihangir.dao.TravelDAO;
import com.cihangir.model.Travel;
import com.cihangir.service.ITravelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;



@Service
@Transactional
public class TravelService implements ITravelService {

    @Autowired
    private TravelDAO travelDAO;

    @Override
    public List<Travel> getTravelByUserId(Long theId) {
        return travelDAO.findTravelByUserId(theId);
    }

    @Override
    public void saveTravel(Travel theTravel) {
        travelDAO.saveTravel(theTravel);
    }

    @Override
    public Travel findTravelById(Long theId) {
        return travelDAO.getTravelById(theId);
    }

    @Override
    public void deleteTravel(Long theId) {
        travelDAO.deleteTravel(theId);
    }

    @Override
    public List<Travel> getTravelsBetweenDateByUserId(Date d1, Date d2, Long id) {
        return travelDAO.findTravelsBetweenDatesWithUserId(d1, d2, id);
    }

    @Override
    public List<Travel> findAllTravels() {
        return travelDAO.getAllTravels();
    }

    @Override
    public List<Travel> getTravelsBetweenDates(Date d1, Date d2) {
        return travelDAO.findTravelBetweenDate(d1, d2);
    }

    @Override
    public void deleteTravelByUserId(Long theId) {
        travelDAO.deleteTravelWithUserId(theId);
    }

}
