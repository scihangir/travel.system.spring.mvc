package com.cihangir.service.impl;

import com.cihangir.dao.UserDAO;
import com.cihangir.model.User;
import com.cihangir.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class UserService implements IUserService {

    @Autowired
    private UserDAO userDAO;

    public User findUserByName(String username) {
        return userDAO.findUserByName(username);
    }

    @Override
    public List<User> getAllUser() {
        return userDAO.findAllUser();
    }

    @Override
    public User findUserById(Long id) {
        return userDAO.findUserById(id);
    }


    @Override
    public void saveUser(User user) {
        userDAO.insertUser(user);
    }

    @Override
    public void deleteUser(Long theId) {
        userDAO.deleteUser(theId);
    }


}
