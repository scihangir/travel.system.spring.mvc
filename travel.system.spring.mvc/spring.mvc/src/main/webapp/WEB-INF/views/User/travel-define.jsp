<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta charset="utf-8" />
    <title>Seyahat Tanımlama</title>


    <%--Css Libraries--%>
    <link rel="stylesheet" href="${contextPath}/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/jquery/css/jquery-ui.css">
    <link  rel="stylesheet" href="${contextPath}/resources/validator/bootstrapValidator.css">



    <%--Js Libraries--%>
    <script src="${contextPath}/resources/jquery/js/jquery-1.12.4.js"></script>
    <script src="${contextPath}/resources/jquery/js/jquery-ui.js"></script>
    <script src="${contextPath}/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="${contextPath}/resources/jquery/i18n/datepicker-tr.js"></script>


    <%--Validator Libraries--%>
    <script src="${contextPath}/resources/validator/bootstrapValidator.js"></script>
    <script src="${contextPath}/resources/validator/jquery.validate.js"></script>
    <script src="${contextPath}/resources/validator/jquery.validate.min.js"></script>




    <script>
        $( function() {
            $( "#startDate" ).datepicker({
                minDate: 0,
                changeMonth:true,
                changeYear:true,
                onSelect: function(selected) {
                    $("#endDate").datepicker("option", "minDate", selected)
                }
            });
            $( "#endDate" ).datepicker({
                changeMonth:true,
                changeYear:true,
                onSelect: function(selected) {
                    $("#startDate").datepicker("option","maxDate", selected)
                }

            });


            var validator = $("#form-validation").bootstrapValidator({
                fields : {
                    destination :{
                        validators : {
                            notEmpty : {
                                message : "Seyahat yeri giriniz."
                            },
                            stringLength: {
                                min: 3,
                                max: 20,
                                message:'Please enter at least 3 characters and no more than 20'
                            }

                        }
                    },
                    purpose :{
                        validators : {
                            notEmpty : {
                                message : "Gidiş amacını giriniz."
                            }
                        }, stringLength: {
                            min: 5,
                            max: 20,
                            message:'Please enter at least 5 characters and no more than 20'
                        }
                    },
                    projectCode :{
                        validators : {
                            notEmpty : {
                                message : "Proje kodunu giriniz"
                            }
                        }
                    },
                    cost :{
                        validators : {
                            notEmpty : {
                                message : "Maliyeti giriniz."
                            },
                            integer: {
                                message: 'Sadece sayı giriniz'
                            }
                        }
                    }
                }
            });

            $('#form-validation').validate({
                rules: {
                    startDate: {
                        required: true
                    },
                    endDate: {
                        required: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.control-group').removeClass('success').addClass('error');
                },
                success: function(element) {
                    element
                            .text('OK!').addClass('valid')
                            .closest('.control-group').removeClass('error').addClass('success');
                }
            });


        } );
    </script>

</head>
<body>
<jsp:include page="../User/navbar.jsp"/>
<br/><br/><br/>
<div class="row">
    <div class="col-lg-5 col-lg-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>Seyahat Detayı</strong>
            </div>

            <div class="panel-body">
                <form:form class="form-horizontal" id="form-validation" action="${pageContext.request.contextPath}/user/saveTravel" modelAttribute="newTravel">
                    <!-- need to associate this data with travel id -->
                    <form:hidden path="id" />
                    <div class="form-group">
                        <label class="col-sm-4 col-md-3 col-lg-3 control-label">Seyahat Yeri</label>
                        <div class="col-sm-8 col-md-6 col-lg-5">
                            <form:input type="text" class="form-control" name="destination" path="destination"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 col-md-3 col-lg-3 control-label">Gidiş Amacı</label>
                        <div class="col-sm-8 col-md-6 col-lg-5">
                            <form:input type="text" class="form-control" name="purpose" path="purpose"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 col-md-3 col-lg-3 control-label">Proje Kodu</label>
                        <div class="col-sm-8 col-md-6 col-lg-5">
                            <form:input type="text" class="form-control" name="projectCode" path="projectCode"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 col-md-3 col-lg-3 control-label">Maliyet</label>
                        <div class="col-sm-6 col-md-4 col-lg-2">
                            <form:input type="text" class="form-control" name="cost" path="cost"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 col-md-3 col-lg-3 control-label">Seyahat Başlangıcı</label>
                        <div class="col-sm-8 col-md-6 col-lg-5">
                            <form:input type="text" id="startDate" path="startDate" name="startDate"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 col-md-3 col-lg-3 control-label">Seyahat Sonu</label>
                        <div class="col-sm-8 col-md-6 col-lg-5">
                            <form:input type="text" id="endDate" path="endDate"  name="endDate" />
                        </div>
                    </div>
                    <br/>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-md-offset-3 col-lg-offset-3 col-sm-8 col-md-9 col-lg-9">
                            <button type="submit" class="btn btn-primary">İptal</button>
                            <button type="submit" class="btn btn-primary">Kaydet</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>

</body>
</html>
