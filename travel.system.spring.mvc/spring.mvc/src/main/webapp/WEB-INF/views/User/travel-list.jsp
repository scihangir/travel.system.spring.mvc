<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta charset="utf-8" />
    <title>Seyahat Tanımlama</title>

    <%--Css Libraries--%>
    <link rel="stylesheet" href="${contextPath}/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/jquery/css/jquery-ui.css">
    <link  rel="stylesheet" href="${contextPath}/resources/validator/bootstrapValidator.css">



    <%--Js Libraries--%>
    <script src="${contextPath}/resources/jquery/js/jquery-1.12.4.js"></script>
    <script src="${contextPath}/resources/jquery/js/jquery-ui.js"></script>
    <script src="${contextPath}/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="${contextPath}/resources/jquery/i18n/datepicker-tr.js"></script>


    <%--Validator Libraries--%>
    <script src="${contextPath}/resources/validator/bootstrapValidator.js"></script>
    <script src="${contextPath}/resources/validator/jquery.validate.js"></script>
    <script src="${contextPath}/resources/validator/jquery.validate.min.js"></script>



    <script>
        $( function() {
            $( "#sDate" ).datepicker({
                changeMonth:true,
                changeYear:true,
                onSelect: function(selected) {
                    $("#eDate").datepicker("option", "minDate", selected)
                }
            });
            $( "#eDate" ).datepicker({
                changeMonth:true,
                changeYear:true,
                onSelect: function(selected) {
                    $("#sDate").datepicker("option","maxDate", selected)
                }
            });

            $('#form-validation').validate({
                rules: {
                    sDate: {
                        required: true
                    },
                    eDate: {
                        required: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.control-group').removeClass('success').addClass('error');
                }
            });


        } );
    </script>



</head>
<body>
<jsp:include page="../User/navbar.jsp"/>
<div class="container table-responsive">
    <div class="row">
        <form class="form-inline" action="${contextPath}/user/search" id="form-validation">
            <div class="form-group">
                <input type="text" id="sDate" name="sDate" placeholder="Başlangıç">
            </div>
            <div class="form-group">
                <input type="text" id="eDate" name="eDate" placeholder="Bitiş">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </div>
        </form>
    </div>
    <div class="row">
        <a class="btn btn-info btn-xs pull-right" href="${contextPath}/user/showFormForAdd" >
            <span class="glyphicon glyphicon-plus-sign"></span>
        </a>
    </div>
    <div class="row">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Bölümü</th>
                <th>Bölüm Müdürü</th>
                <th>Seyahat Başlangcıcı</th>
                <th>Seyahat Sonu</th>
                <th>Seyahat Yeri</th>
                <th>Gidiş Amacı</th>
                <th>Proje Kodu</th>
                <th>Maliyet</th>
                <th class="text-center">İşlemler</th>
            </tr>
            </thead>
            <tbody>
            <!-- loop over and print our customers -->
            <c:forEach items="${travels}" var="travel">

                <!-- construct an "update" link with travek id -->
                <c:url var="updateLink" value="${contextPath}/user/showFormForEdit">
                    <c:param name="travelId" value="${travel.id}" />
                </c:url>

                <!-- construct an "delete" link with travel id -->
                <c:url var="deleteLink" value="${contextPath}/user/deleteTravel">
                    <c:param name="travelId" value="${travel.id}" />
                </c:url>

                <tr>
                    <td>
                        <c:out value="${travel.user.department}" />
                    </td>
                    <td>
                        <c:out value="${travel.user.departmentHeader}" />
                    </td>
                    <td>
                        <fmt:formatDate pattern = "dd.MM.yyyy" value = "${travel.startDate}" />
                    </td>
                    <td>
                        <fmt:formatDate pattern = "dd.MM.yyyy" value = "${travel.endDate}" />
                    </td>
                    <td>
                        <c:out value="${travel.destination}" />
                    </td>
                    <td>
                        <c:out value="${travel.purpose}" />
                    </td>
                    <td>
                        <c:out value="${travel.projectCode}"/>
                    </td>
                    <td>
                        <c:out value="${travel.cost}" />
                    </td>
                    <td class="text-center">
                        <a href="${updateLink}" class="btn btn-success btn-xs">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <a href="${deleteLink}"
                           onclick="return confirm('Bu seyahati silmek istediğinize emin misiz?')" class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
