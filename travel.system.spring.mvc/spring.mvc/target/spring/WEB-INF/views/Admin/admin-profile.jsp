
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta charset="utf-8" />
    <title>Admin Profil</title>


    <%--Css Libraries--%>
    <link rel="stylesheet" href="${contextPath}/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/jquery/css/jquery-ui.css">
    <link  rel="stylesheet" href="${contextPath}/resources/validator/bootstrapValidator.css">


    <%--Js Libraries--%>
    <script src="${contextPath}/resources/jquery/js/jquery-1.12.4.js"></script>
    <script src="${contextPath}/resources/jquery/js/jquery-ui.js"></script>
    <script src="${contextPath}/resources/bootstrap/js/bootstrap.min.js"></script>

    <%--Validator Libraries--%>
    <script src="${contextPath}/resources/validator/bootstrapValidator.js"></script>
    <script src="${contextPath}/resources/validator/jquery.validate.js"></script>
    <script src="${contextPath}/resources/validator/jquery.validate.min.js"></script>

    <script>
        $(document).ready(function(){


            var validator = $("#form-validation").bootstrapValidator({
                fields : {
                    username :{
                        validators : {
                            notEmpty : {
                                message : "Kullanıcı adını giriniz."
                            },
                            stringLength: {
                                min: 5,
                                max: 20,
                                message:'Please enter at least 5 characters and no more than 20'
                            }

                        }
                    },
                    password :{
                        validators : {
                            notEmpty : {
                                message : "Şifre giriniz."
                            }
                        }, stringLength: {
                            min: 5,
                            max: 20,
                            message:'Please enter at least 5 characters and no more than 20'
                        }
                    },
                    registrationNumber :{
                        validators : {
                            notEmpty : {
                                message : "Sicil Numarası Giriniz"
                            },
                            integer: {
                                message: 'Sicil numarası rakamlardan oluşmaktadır.'
                            }
                        }
                    },
                    department :{
                        validators : {
                            notEmpty : {
                                message : "Bölüm giriniz."
                            }
                        }
                    }
                }
            });


        });
    </script>

    <style>
        #seyahatDetayi{
            margin-top:80px;
            width: 800px;

        }
    </style>
</head>
<body>
<jsp:include page="../Admin/navbar.jsp"/>
<div class="container" id="seyahatDetayi">
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong>Admin Bilgileri</strong>
        </div>

        <div class="panel-body">
            <form:form class="form-horizontal" id="form-validation" method="post" action="${pageContext.request.contextPath}/admin/updateAdmin" modelAttribute="editAdmin">
                <!-- need to associate this data with customer id -->
                <form:hidden path="id" />
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-3 control-label">Kullanıcı Adı</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" name="username" path="username"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-3 control-label">Şifre</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" name="password" path="password"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-3 control-label">Sicil Numarası</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" name="registrationNumber" path="registrationNumber"/>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-4 col-md-3 col-lg-3 control-label">Bölümü</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" name="deparment" path="department"/>
                    </div>
                </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-md-offset-3 col-lg-offset-3 col-sm-8 col-md-9 col-lg-9">
                            <button type="submit" class="btn btn-primary">İptal</button>
                            <button type="submit" class="btn btn-primary">Güncelle</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
