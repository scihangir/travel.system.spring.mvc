<%--
  Created by IntelliJ IDEA.
  User: cihangir
  Date: 6/12/17
  Time: 3:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <title>Admin</title>
    <link rel="stylesheet" href="${contextPath}/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/jquery/css/jquery-ui.css">

    <script src="${contextPath}/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="${contextPath}/resources/jquery/js/jquery-1.12.4.js"></script>
    <script src="${contextPath}/resources/jquery/js/jquery-ui.js"></script>


    <style>
        #datatable{
            margin-top: 50px;
            margin-left: 30px;
            margin-right: 30px;
        }

        #add{
            margin-top: 30px;
            margin-bottom: 2px;
            margin-right: 20px;
        }

        #welcomeUser{
            margin-left: 1425px;
            margin-top:10px;
        }

    </style>
</head>
<body>
<div class="page-header">
   <div class="pull-right">
       <div class="btn-group">
           <button type="button" class="btn btn-info btn-radius height-equal">
            <span>
                <i class="glyphicon glyphicon-user"></i>
                <span class="hidden-xs"> <c:out value="${authenticatedAdmin}" /><span class="caret"></span></span>
            </span>
           </button>
       </div>
   </div>
</div>

<div class="pull-right" id="add">
    <a class="btn btn-info btn-xs" href="${contextPath}/admin/insertUser">
        <span class="glyphicon glyphicon-plus"></span>
    </a>
</div>
<div id="datatable">
    <div class="row">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Kullanıcı Adı</th>
                <th>Bölümü</th>
                <th>Bölüm Müdürü</th>
                <th>Rolü</th>
                <th>Seyahat Bilgilendirme</th>
                <th class="text-center">İşlemler</th>
            </tr>
            </thead>
            <tbody>
                <!-- Kayitlari listeleyelim... -->
                <c:forEach items="${users}" var="user">
                    <tr>
                        <td>
                            <c:out value="${user.username}" />
                        </td>
                        <td>
                            <c:out value="${user.department}" />
                        </td>
                        <td>
                            <c:out value="${user.departmentHeader}" />
                        </td>
                        <td>
                            <c:out value="${user.role.name}" />
                        </td>
                        <td>
                            <c:out value="${user.period }-${user.dayOfPeriod }"/>
                        </td>
                        <td class="text-center">
                            <a href="${contextPath}/admin/editUser?userId=<c:out value='${user.id}'/>" class="btn btn-success btn-xs">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                            <a href="${contextPath}/admin/deleteUser?userId=<c:out value='${user.id}'/>" onclick="return confirm('Bu kullanıcıyı silmek istediğinize emin misiz?')" class="btn btn-danger btn-xs">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
