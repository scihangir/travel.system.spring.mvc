<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta charset="utf-8" />
    <title>Kullanıcı Tanımlama</title>

    <link rel="stylesheet" href="${contextPath}/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/jquery/css/jquery-ui.css">


    <script src="${contextPath}/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="${contextPath}/resources/jquery/js/jquery-1.12.4.js"></script>
    <script src="${contextPath}/resources/jquery/js/jquery-ui.js"></script>


    <script>
        $(document).ready(function(){
            $('#bilgilendirmeCheckBox').click(function () {
                $('#haftalik').prop('disabled',!(this.checked));
                $('#aylik').prop('disabled',!(this.checked));
                $('#gunler').prop('disabled',!(this.checked));

            });
        });
    </script>

    <style>
        #seyahatDetayi{
            margin-top:80px;
            width: 800px;

        }
        #bilgilendirmeServisi {
            margin-left: 20px;
            width: 600px;
        }
        #seyahatSistemi {
            margin-top: 10px;
            margin-left: 1px;
            width: 450px;
        }
    </style>
</head>
<body>
<div class="container" id="seyahatDetayi">
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong>Kullanıcı Bilgileri</strong>
        </div>

        <div class="panel-body">
            <form:form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/admin/saveUser" modelAttribute="newUser">
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-3 control-label">İsim</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" id="firstname" path="firstname"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-3 control-label">Soysim</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" id="firstname" path="lastname"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-3 control-label">Kullanıcı Adı</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" id="username" path="username"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-3 control-label">Şifre</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" id="password" path="password"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-3 control-label">Sicil Numarası</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" id="registrationNumber" path="registrationNumber"/>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-4 col-md-3 col-lg-3 control-label">Bölümü</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" id="deparment" path="department"/>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-4 col-md-3 col-lg-3 control-label">Bölüm Müdürü</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" id="departmentHeader" path="departmentHeader"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-3 control-label">Rölü</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:select class="form-control" type="text" id="role" path="role">
                            <form:option value="ROLE_USER">Normal Kullanıcı</form:option>
                            <form:option value="ROLE_ADMIN">Admin</form:option>
                        </form:select>
                    </div>
                </div>
                <div class="form-group" id="bilgilendirmeServisi">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>Bilgilendirme Servisi</strong>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3">
                                    <form:checkbox id="bilgilendirmeCheckBox" path="informing" value="Açık"/>Açık
                                </label>
                            </div>

                            <div class="form-group" id="seyahatSistemi">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <strong>Seyahat Sistemi</strong>
                                    </div>

                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-8 col-md-6 col-lg-5">
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label>
                                                        <form:radiobutton  id="haftalik" path="period" value="Haftalık" disabled="true"/>Haftalık
                                                    </label>
                                                    <label>
                                                        <form:radiobutton  id="aylik" path="period"  value="Aylık" disabled="true" />Aylık
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-8 col-md-6 col-lg-5">
                                                <form:select class="form-control" id="gunler" path="dayOfPeriod" disabled="true">
                                                    <form:option value="Pazartesi">Pazartesi</form:option>
                                                    <form:option value="Salı">Salı</form:option>
                                                    <form:option value="Çarşamba">Çarşamba</form:option>
                                                    <form:option value="Perşembe">Perşembe</form:option>
                                                    <form:option value="Cuma">Cuma</form:option>
                                                    <form:option value="Cumartesi">Cumartesi</form:option>
                                                    <form:option value="Pazar">Pazar</form:option>
                                                </form:select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-md-offset-3 col-lg-offset-3 col-sm-8 col-md-9 col-lg-9">
                            <button type="submit" class="btn btn-primary">İptal</button>
                            <button type="submit" class="btn btn-primary">Kaydet</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
