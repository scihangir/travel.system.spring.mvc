<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta charset="utf-8" />
    <title>Kullanıcı Tanımlama</title>

    <%--Css Libraries--%>
    <link rel="stylesheet" href="${contextPath}/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/jquery/css/jquery-ui.css">
    <link  rel="stylesheet" href="${contextPath}/resources/validator/bootstrapValidator.css">



    <%--Js Libraries--%>
    <script src="${contextPath}/resources/jquery/js/jquery-1.12.4.js"></script>
    <script src="${contextPath}/resources/jquery/js/jquery-ui.js"></script>
    <script src="${contextPath}/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="${contextPath}/resources/jquery/i18n/datepicker-tr.js"></script>


    <%--Validator Libraries--%>
    <script src="${contextPath}/resources/validator/bootstrapValidator.js"></script>
    <script src="${contextPath}/resources/validator/jquery.validate.js"></script>
    <script src="${contextPath}/resources/validator/jquery.validate.min.js"></script>



    <script>
        $(document).ready(function(){
            $('#bilgilendirmeCheckBox').click(function () {
                $('#haftalik').prop('disabled',!(this.checked));
                $('#aylik').prop('disabled',!(this.checked));
                $('#gunler').prop('disabled',!(this.checked));

            });
            var validator = $("#form-validation").bootstrapValidator({
                fields : {
                    username :{
                        validators : {
                            notEmpty : {
                                message : "Kullanıcı adını giriniz."
                            },
                            stringLength: {
                                min: 5,
                                max: 20,
                                message:'Please enter at least 5 characters and no more than 20'
                            }

                        }
                    },
                    password :{
                        validators : {
                            notEmpty : {
                                message : "Şifre giriniz."
                            }
                        }, stringLength: {
                            min: 5,
                            max: 20,
                            message:'Please enter at least 5 characters and no more than 20'
                        }
                    },
                    registrationNumber :{
                        validators : {
                            notEmpty : {
                                message : "Sicil Numarası Giriniz"
                            },
                            integer: {
                                message: 'Sicil numarası rakamlardan oluşmaktadır.'
                            }
                        }
                    },
                    department :{
                        validators : {
                            notEmpty : {
                                message : "Bölüm giriniz."
                            }
                        }
                    },
                    departmentHeader :{
                        validators : {
                            notEmpty : {
                                message : "Bölüm müdürünü giriniz."
                            },
                            stringLength: {
                                min: 5,
                                max: 20,
                                message:'Please enter at least 5 characters and no more than 20'
                            }

                        }
                    }
                }
            });
        });
    </script>

    <style>
        #seyahatDetayi{
            margin-top:80px;
            width: 800px;

        }
        #bilgilendirmeServisi {
            margin-left: 20px;
            width: 600px;
        }
        #seyahatSistemi {
            margin-top: 10px;
            margin-left: 1px;
            width: 450px;
        }
    </style>
</head>
<body>
<jsp:include page="../Admin/navbar.jsp"/>
<div class="container" id="seyahatDetayi">
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong>Kullanıcı Bilgileri</strong>
        </div>

        <div class="panel-body">
            <form:form class="form-horizontal" method="post" id="form-validation" action="${pageContext.request.contextPath}/admin/saveUser" modelAttribute="newUser">
                <!-- need to associate this data with user id -->
                <form:hidden path="id" />
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-3 control-label">Kullanıcı Adı</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" name="username" path="username"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-3 control-label">Şifre</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" name="password" path="password"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-3 control-label">Sicil Numarası</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" name="registrationNumber" path="registrationNumber"/>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-4 col-md-3 col-lg-3 control-label">Bölümü</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" name="deparment" path="department"/>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-4 col-md-3 col-lg-3 control-label">Bölüm Müdürü</label>
                    <div class="col-sm-8 col-md-6 col-lg-5">
                        <form:input type="text" class="form-control" name="departmentHeader" path="departmentHeader"/>
                    </div>
                </div>
                <div class="form-group" id="bilgilendirmeServisi">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>Bilgilendirme Servisi</strong>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3">
                                    <form:checkbox id="bilgilendirmeCheckBox" path="informing" value="Açık"/>Açık
                                </label>
                            </div>

                            <div class="form-group" id="seyahatSistemi">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <strong>Seyahat Sistemi</strong>
                                    </div>

                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-8 col-md-6 col-lg-5">
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label>
                                                        <form:radiobutton  id="haftalik" path="period" value="Haftalık" disabled="true"/>Haftalık
                                                    </label>
                                                    <label>
                                                        <form:radiobutton  id="aylik" path="period"  value="Aylık" disabled="true" />Aylık
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-8 col-md-6 col-lg-5">
                                                <form:select class="form-control" id="gunler" path="dayOfPeriod" disabled="true">
                                                    <form:option value="Pazartesi">Pazartesi</form:option>
                                                    <form:option value="Salı">Salı</form:option>
                                                    <form:option value="Çarşamba">Çarşamba</form:option>
                                                    <form:option value="Perşembe">Perşembe</form:option>
                                                    <form:option value="Cuma">Cuma</form:option>
                                                    <form:option value="Cumartesi">Cumartesi</form:option>
                                                    <form:option value="Pazar">Pazar</form:option>
                                                </form:select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-md-offset-3 col-lg-offset-3 col-sm-8 col-md-9 col-lg-9">
                            <button type="submit" class="btn btn-primary">İptal</button>
                            <button type="submit" class="btn btn-primary">Kaydet</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
